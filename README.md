# LiberaForms cluster

Use this repository for docker orchestration of multiple LiberaForms containers. This allows you to host LiberaForms for various tenants where each tenant has a unique FQDN (Fully Qualified Domain Name) like forms.example.com

## Requirements

Install `docker` and `docker-compose`


## Containers

Database server: A PostgreSQL server is shared by the tenants. It contains one database for each tenant.

Static files: Run this container to copy static files into the volume `static-files-vol` used by Nginx.

Web server: Nginx, and companions, serve the tenants' LiberaForms web sites, manage vhost configurations, and LetsEncrypt certificates.

Memcached server: LiberaForms uses this server to store session data.

LiberaForms servers: One container is instantiated for each tenant.


## Installation

Create a directory somewhere on your server's filesystem and clone these two repositories.

* The `cluster` project (this project) is used to manage the docker containers
* The `liberaforms` project is used to build the LiberaForms docker image(s)

```
mkdir liberaforms
cd liberaforms
git clone https://gitlab.com/liberaforms/cluster.git cluster
git clone https://gitlab.com/liberaforms/liberaforms.git liberaforms
```

Create the cluster's python virtual environment.

```
apt-get install python3-venv cryptography
cd ./cluster
python3 -m venv ./venv
source ./venv/bin/activate
pip install --upgrade pip
pip install -r ./requirements.txt
```

## Networks

```
docker network create -d bridge back-tier
```

## Volumes

```
docker volume create --name=static-files-vol
```

Containers are ephemeral. However, we need some of the tentants' files to persist indepenently of the containers.

* `uploads`: A directory where media and form attachments are saved
* `logs`: A directory where LiberaForms server logs are saved
* `database backups`: A directory where PostgreSQL db dumps will be saved

Decide on the paths and create the directories.

```
mkdir /opt/liberaforms_uploads
mkdir /opt/liberaforms_logs
mkdir /var/backups/postgres
```

## .env

Create the `.env` file. This file contains basic environment variables.

```
cp dotenv.example .env
```

Edit the `.env` file and edit the if needed.

```
UPLOADS_DIR=/opt/liberaforms_uploads
LOGS_DIR=/opt/liberaforms_logs
POSTGRES_BACKUPS_DIR=/var/backups/postgres
```

Also add your email address at

```
LETSENCRYPT_EMAIL="your_email@example.com"
```

## Liberaforms docker image

If you haven't created the image, now is a good time.

```
cd ../liberaforms
docker build -t liberaforms-app:$(cat VERSION.txt) ./
```

After doing that you can see the image like this.
```
docker image list liberaforms-app
```

## `manage.py`

Cluster management is done using `manage.py`

Note: The following commands require you to source the python virtual environment.

```
cd ./cluster
source venv/bin/activate
```

## Database server

A database server contains multiple databases, one for each tenant.

Create the PostgreSQL server's docker-compose configuration.

```
./manage.py postgres create-config
```

The container name is `POSTGRES_CONTAINER_NAME` as defined in your `.env` file.

These files are created:

* `./postgres/<POSTGRES_CONTAINER_NAME>/environment.ini`
* `./postgres/<POSTGRES_CONTAINER_NAME>/docker-compose.yml`


### Start/stop the database server

Start the server.
```
./manage.py postgres compose up [-d]
```

Stop the server.
```
./manage.py postgres compose down
```

Note that the `POSTGRES_ROOT_USER` and `POSTGRES_ROOT_PASSWORD` are only used when the
PostgreSQL container is created the first time.

## Static files volume

Use this container to copy a `VERSION` of static files into the volume `static-files-vol`.

Create the image. You only need to do this once.

```
cd ./static_files
docker build -t liberaforms-static .
```

Run the image.

```
docker run -v static-files-vol:/opt/static -it --entrypoint /bin/bash liberaforms-static
```

Now from inside the container, where `VERSION` is the LiberaForms' repository tag.

```
VERSION=2.0.0 clone_static.sh
```

You can also `cd` into `/opt/static` and delete older, unused versions.

## Web server

The web server consists of three containers:

* `nginx-proxy`: Serves the LiberaForms web sites
* `proxy-dockergen`: Creates vhosts configurations
* `proxy-letsencrypt`: Manages LetsEncrypt certificates

Create the web server's `docker-compose.yml` file

```
./manage.py nginx create-config
```

The file `./nginx/docker-compose.yml` is created. Edit if needed.


### Start/stop

Start nginx-proxy, proxy-dockergen, and proxy-letsencrypt containers.

```
./manage.py nginx compose up [-d]
```

Stop the containers.

```
./manage.py nginx compose stop
```

## Memcached

Each LiberaForms container stores it's session data on a memcached server.

Start the container.

```
./manage.py memcached compose up -d
```

## Tenants

A tentant is an entity with a FQDN who is hosted on your infrastructure.
Each tenant has a unique database on the database server.
One LiberaForms container is intantiated for each tenant.

### Tenant container

A Tenant container serves LiberaForms.

Create a tenant's docker-compose configuration.

* `tenant_name`: The name of the organization you are hosting for.
* `--base-url`: The base url of the LiberaForms website
* `--admin-email`: The email address of the first admin user.

```
./manage.py tenant <tenant_name> config-create --base-url=https://forms.example.com --admin-email=nobody@example.com
```
The directory `./tenants/<tenant_name>` is created.

> It is important to keep backups of tenant configurations.

Please revise the environment file `./tenants/<tenant_name>/environment.ini`.
Change `LIBERAFORMS_VERSION` to the image tag of the version you want to use.

List available image versions like this
```
docker image list liberaforms-app
```

### Tenant database

Each tenant requires a database.

Create the database.
```
./manage.py tenant <tenant_name> database-create
```

You can delete the database if needed.

```
./manage.py tenant <tenant_name> database-drop
```

#### Start/stop a tenant container

Start the container
```
./manage.py tenant <tenant_name> compose up [-d]
```

Stop the container
```
./manage.py tenant <tenant_name> compose down
```

#### Populate the database with the schema

For a new installation

```
./manage.py tenant <tenant_name> flask database create-tables
```

If you are migrating an installation and the database already exists with data.

```
./manage.py tenant <tenant_name> flask db upgrade
#./manage.py tenant <tenant_name> database-upgrade
```

### Restore a tenant's database

```
./manage.py tenant <tenant_name> database-drop
./manage.py tenant <tenant_name> database-create
./manage.py postgres restore --tenant=<tenant_name> --dump-file=/var/backups/postgres/<db_name>.dump.sql
```

## Backups

You should backup:

* `./tenants` directory. Contains configuration of LiberaForms containers. You definitely do not want to lose `CRYPTO_KEY` if you have set `ENABLE_UPLOADS=True`!
* `UPLOADS_DIR` directory. Contains media and form attachments.

## PostgreSQL

Create a directory for the backup and set permissions

```
mkdir /var/backups/postgres
```

Run this and check if a copy is dumped correctly.
```
./venv/bin/python3 ./manage.py postgres backup --tenant=<tenant_name>
ls -l /var/backups/postgres
```

Add a line to your crontab
```
crontab -e
```
to run it every night..
```
30 3 * * * cd /path/to/cluster && ./venv/bin/python3 ./manage.py postgres backup --tenant=<tenant_name>
```

Note: This overwrites the last copy. You might want to change that.

Don't forget to check that the cronjob is dumping the database correctly.

## Restore a tenant's database

```
./manage.py tenant <tenant_name> database-drop
./manage.py tenant <tenant_name> database-create
./manage.py postgres restore --tenant=<tenant_name> --dump-file=/var/backups/postgres/<db_name>.dump.sql
```

# Upgrade

## Cluster

```
cd cluster
git pull origin main
```

## LiberaForms app

```
cd liberaforms
git fetch origin tag <version> --no-tags
git checkout <version> -b <version>
docker build -t liberaforms-app:$(cat VERSION.txt) ./
```

### Static files

Pull the static files (see above)

### Upgrade tenant

Edit the tenant's `environment.ini` and change the version

Restart the tenant's container

```
./manage.py tenant <tenant_name> compose down
./manage.py tenant <tenant_name> compose up -d
```

Upgrade the database

```
./manage.py tenant <tenant_name> flask db upgrade
```

### Downgrade the tenant

Edit the tenant's `environment.ini` and change the version

Restart the tenant's container


# Development


## Create a tenant for development

Create a domain name. Edit `/etc/hosts` and add an entry like this

```
127.0.0.1	<tenant_name>.localhost
```

```
./manage.py tenant <tenant_name> config-create --base-url=<tenant_name>.localhost --admin-email=nobody@example.com

```
### Create the tenant's database

```
./manage.py tenant <tenant_name> database-create
```

## Watch the logs

```
docker logs -f <tenant api container name>
```

## Localhost certs

You can use the self signed default cert for https local host container development

`.env`

```
# True to use default cert (helpful when developing on localhost)
NGINX_USE_DEFAULT_CERT=False
```

Or edit `nginx/docker-compose.yml` if you have already created it.


## Cookies

If you have cookie problems try adding this to your `<tenant_name>`'s `environment.ini` file

```
HTTPS_ONLY=False
```
