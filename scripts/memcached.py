"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import click
from scripts import utils

def compose(compose_args):
    docker_compose_file = f"./memcached/docker-compose.yml"
    compose_cmdline = [os.environ['DOCKER_COMPOSE_BIN'], "-f", docker_compose_file]
    compose_cmdline = compose_cmdline + list(compose_args)
    click.echo(" ".join(compose_cmdline))
    utils.run_subprocess(compose_cmdline)
