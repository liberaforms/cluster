"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import string
import secrets
import signal
import subprocess


def run_subprocess(cmdline, env={}):
    try:
        p = subprocess.Popen(cmdline, env=env)
        p.wait()
    except KeyboardInterrupt:
        p.send_signal(signal.SIGINT)
        p.wait()

def random_string():
    chars = string.ascii_letters + string.digits
    return ''.join(secrets.choice(chars) for i in range(20))
