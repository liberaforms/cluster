#!/bin/bash

work_dir="/storage/snapshots"
suffix="_victoriadb_backup.tgz"

docker exec monitor-victoriametrics wget -q -O- localhost:8428/snapshot/delete_all
snapshot_name=$(docker exec monitor-victoriametrics  wget -q -O- localhost:8428/snapshot/create | jq '.snapshot' | tr -d '"')

docker exec --workdir $work_dir monitor-victoriametrics sh -c "cp -a -L $snapshot_name $snapshot_name-dereferenced"
docker cp monitor-victoriametrics:/storage/snapshots/$snapshot_name-dereferenced /tmp/$snapshot_name
docker exec --workdir $work_dir monitor-victoriametrics sh -c "rm -fr *dereferenced"

rm -fr /var/backups/victoria_db/*$suffix
tar czf /var/backups/victoria_db/$snapshot_name$suffix /tmp/$snapshot_name
rm -fr /tmp/$snapshot_name
