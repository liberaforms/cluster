"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os, shutil
import click
import subprocess
import click
#from pathlib import Path
from jinja2 import Environment, FileSystemLoader
from dotenv import load_dotenv
from scripts import constants, utils

load_dotenv(dotenv_path="./.env")

db_container_name = os.environ['POSTGRES_CONTAINER_NAME']

def create_config():
    container_dir = f"./postgres/{db_container_name}"
    if not os.path.isdir(container_dir):
        os.mkdir(container_dir)

    # create ./postgres/{db_container_name}/docker-compose.yml
    template = "./postgres/templates/docker-compose.yml"
    docker_compose_file = f"{container_dir}/docker-compose.yml"
    if os.path.isfile(docker_compose_file):
        click.echo(f"'{docker_compose_file}' already exists.")
    else:
        shutil.copyfile(template, docker_compose_file)
        click.echo(f"Created '{docker_compose_file}'")

    # create ./postgres/{db_container_name}/environment.ini
    env_file = f"{container_dir}/environment.ini"
    if os.path.isfile(env_file):
        click.echo(f"'{env_file}' already exists.")
    else:
        ini_files = [
                        './postgres/templates/environment.j2',
                        #'./environment/templates/manager.j2'
                    ]
        tmp_dir = os.getenv("TMP_DIR")
        tmp_template_name = f"{utils.random_string()}.j2"
        tmp_template_file = f"{tmp_dir}/{tmp_template_name}"
        with open(tmp_template_file,'wb') as wfd:
            for f in ini_files:
                with open(f,'rb') as fd:
                    shutil.copyfileobj(fd, wfd)
        j2_env = Environment(loader = FileSystemLoader(tmp_dir))
        template = j2_env.get_template(tmp_template_name)

        network = constants.database_network_name(db_container_name)
        volume = constants.database_volume_name(db_container_name)
        password = utils.random_string()
        environment = template.render({
                            'container_name': db_container_name,
                            'DATABASE_NETWORK': network,
                            'DATABASE_VOLUME': volume,
                            'POSTGRES_HOSTNAME': db_container_name,
                            'POSTGRES_BACKUPS_DIR': os.environ['POSTGRES_BACKUPS_DIR'],
                            'POSTGRES_ROOT_USER': 'postgres',
                            'POSTGRES_ROOT_PASSWORD': password,
                        })
        os.remove(tmp_template_file)
        with open(env_file, 'w') as fd:
            fd.write(environment)
        click.echo(f"Created '{env_file}'")

    # create docker network
    #network_name = constants.database_network_name(db_container_name)
    #cmdline = [ "docker",
    #            "network", "create",
    #            "-d", "bridge", network_name
    #            ]
    #click.echo(f"Running: {' '.join(cmdline)}")
    #utils.run_subprocess(cmdline)

def compose(compose_args):
    docker_compose_file = f"./postgres/{db_container_name}/docker-compose.yml"
    environment_file = f"./postgres/{db_container_name}/environment.ini"
    docker_compose_cmdline = [
                                os.environ['DOCKER_COMPOSE_BIN'],
                                "-f", docker_compose_file,
                                "--env-file", environment_file
                            ]
    docker_compose_cmdline = docker_compose_cmdline + list(compose_args)
    click.echo(" ".join(docker_compose_cmdline))
    utils.run_subprocess(docker_compose_cmdline)

def get_database_config(tenant_name):
    tenant_config = constants.tenant_config(tenant_name)
    if not tenant_config:
        click.echo(f"Cannot find '{constants.TENANTS_DIR}/{tenant_name}/environment.ini'")
        return
    return {'host': tenant_config['DB_HOST'],
            'database': tenant_config['DB_NAME']
            }

def backup_database(tenant):
    database_config = get_database_config(tenant)
    if not database_config:
        return
    db_name = database_config['database']
    dump_file = f"{db_name}.dump.sql"

    dump_path = f"/var/backups/{dump_file}"
    pg_cmd = f"/usr/local/bin/pg_dump -U postgres -d {db_name} -f {dump_path}"
    docker_cmd = f"docker exec -i {database_config['host']} {pg_cmd}"

    click.echo(f"Dumping database {db_name} to {dump_file}")
    click.echo(docker_cmd)
    utils.run_subprocess(docker_cmd.split())

def restore_database(tenant, dump_file):
    if not os.path.isfile(dump_file):
        click.echo(f"Cannot find '{dump_file}'")
        return
    database_config = get_database_config(tenant)
    if not database_config:
        return
    db_name = database_config['database']

    cat_cmd = f"cat {dump_file}"
    pg_cmd = f"psql -U postgres {db_name}"
    docker_cmd = f"docker exec -i {database_config['host']} {pg_cmd}"

    click.echo(f"Restoring database {db_name} from {dump_file}")
    click.echo(f"{cat_cmd} | {docker_cmd}")

    ps_1 = subprocess.Popen(cat_cmd.split(), stdout=subprocess.PIPE)
    ps_2 = subprocess.Popen(docker_cmd.split(), stdin=ps_1.stdout)
    ps_2.communicate()
