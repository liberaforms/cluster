"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import shutil
import click
from dotenv import load_dotenv
from scripts import utils
from jinja2 import Environment, FileSystemLoader

#load_dotenv(dotenv_path="./.env")

def create_config():
    # create docker-compose.yml
    nginx_compose_file = "./nginx/docker-compose.yml"
    if os.path.isfile(nginx_compose_file):
        click.echo(f"'{nginx_compose_file}' already exists.")
        return
    template_dir = "./nginx/templates"
    j2_env = Environment(loader = FileSystemLoader(template_dir))
    template = j2_env.get_template('docker-compose.j2')
    environment = template.render({
                    'UPLOADS_DIR': os.environ["UPLOADS_DIR"],
                    'LOGS_DIR': os.environ["LOGS_DIR"],
                    'PROXY_NETWORK': os.environ["PROXY_NETWORK"],
                    'LETSENCRYPT_EMAIL': os.environ["LETSENCRYPT_EMAIL"],
                    'MAX_BODY_SIZE': os.environ["NGINX_MAX_BODY_SIZE"],
                    'METRICS_ENDPOINT_POLICY': os.environ["NGINX_METRICS_ENDPOINT_POLICY"],
                    'USE_DEFAULT_CERT': os.environ["NGINX_USE_DEFAULT_CERT"],
                    })
    with open(nginx_compose_file, 'w') as fd:
        fd.write(environment)
    click.echo(f"Created '{nginx_compose_file}'")

def compose(compose_args):
    docker_compose_file = f"./nginx/docker-compose.yml"
    compose_cmdline = [os.environ['DOCKER_COMPOSE_BIN'], "-f", docker_compose_file]
    compose_cmdline = compose_cmdline + list(compose_args)
    click.echo(" ".join(compose_cmdline))
    utils.run_subprocess(compose_cmdline)
