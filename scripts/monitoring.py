"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from os import path as p
import click
from jinja2 import Environment, FileSystemLoader
from scripts import constants, utils

config_dir = "./monitoring/"
config_dir_prometheus = p.join(config_dir, "prometheus")

template_dir = p.join(config_dir, "templates")

def create_config():
    """ Adds tenants to the prometheus configuration."""
    # NOTE: No need to make the docker-compose file a template yet.

    td = constants.TENANTS_DIR
    tenants = []
    for x in os.listdir(td):
        if x.startswith("."):
            continue
        tenant_config = constants.tenant_config(x)
        tenants.append({"FQDN": tenant_config["FQDN"],
                        "container_name": tenant_config["CONTAINER_NAME"]})
    j2_env = Environment(loader = FileSystemLoader(template_dir))
    template = j2_env.get_template('prometheus/prometheus.yml.j2')
    prometheus_config = template.render({
            "tenants": tenants
        })
    outfile = p.join(config_dir_prometheus,"prometheus.yml")
    # File is overwritten with no user confirmation because we need to be able
    # to reload it when we add new tenants
    with open(outfile, 'w') as fd:
        fd.write(prometheus_config)
    click.echo(f"Created prometheus config: '{outfile}'")

def compose(compose_args):
    docker_compose_file = p.join(config_dir, "docker-compose.yml")
    compose_cmdline = [
                        os.environ['DOCKER_COMPOSE_BIN'],
                        "-f", docker_compose_file
                      # "--env-file", environment_file
                      ]
    compose_cmdline = compose_cmdline + list(compose_args)
    click.echo(" ".join(compose_cmdline))
    utils.run_subprocess(compose_cmdline)
