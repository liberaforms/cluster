"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import configparser

TENANTS_DIR = "./tenants"

#def database_hostname(container_name):
#    return f"liberaforms_{container_name}_db"

def database_network_name(container_name):
    return f"{container_name}_net"

def database_volume_name(container_name):
    return f"{container_name}_vol"

def tenant_dir(tenant_name):
    return f"{TENANTS_DIR}/{tenant_name}"

def tenant_container_name(tenant_name):
    return f"LF_{tenant_name}"

def tenant_database_name(domain):
    db_name = f"lf_{domain.replace('.', '_')}"
    return db_name.replace('-', '_')

def tenant_config(tenant_name):
    env_file = f"{tenant_dir(tenant_name)}/environment.ini"
    if not os.path.isfile(env_file):
        return
    with open(env_file) as f:
        # Add a dummy section TENANT, required by configparser.
        tenant_environment = '[TENANT]\n' + f.read()
    config_parser = configparser.RawConfigParser()
    config_parser.read_string(tenant_environment)
    environment = {}
    # Remove "" from values. Can cause problems. eg docker-compose
    # configparser does not respect uppercase.
    for option in config_parser['TENANT']:
        environment[option.upper()] = config_parser['TENANT'][option].strip('"')
    return environment
