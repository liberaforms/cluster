"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os, shutil
from urllib.parse import urlparse
import secrets
import click
from dotenv import load_dotenv
from cryptography.fernet import Fernet
from jinja2 import Environment, FileSystemLoader
from scripts import constants, utils

load_dotenv(dotenv_path="./.env")

def create_config(tenant_name, base_url, admin_email):
    tenant_dir = constants.tenant_dir(tenant_name)
    if not os.path.isdir(tenant_dir):
        os.mkdir(tenant_dir)

    liberaforms_version = "3.0.0"
    template_dir = "./app/templates"
    container_name = constants.tenant_container_name(tenant_name)
    # create docker-compose.yml
    tenant_compose_file = f"{tenant_dir}/docker-compose.yml"
    if os.path.isfile(tenant_compose_file):
        click.echo(f"'{tenant_compose_file}' already exists.")
    else:
        j2_env = Environment(loader = FileSystemLoader(template_dir))
        template = j2_env.get_template('docker-compose.j2')
        db_container_name = os.environ['POSTGRES_CONTAINER_NAME']
        database_network = constants.database_network_name(db_container_name)
        environment = template.render({
                        'CONTAINER_NAME': container_name,
                        'PROXY_NETWORK': os.environ["PROXY_NETWORK"],
                        'UPLOADS_DIR': os.environ['UPLOADS_DIR'],
                        'LOGS_DIR': os.environ['LOGS_DIR']
                        })
        with open(tenant_compose_file, 'w') as fd:
            fd.write(environment)
        click.echo(f"Created '{tenant_compose_file}'")
    # create environment.ini
    tenant_config_file = f"{tenant_dir}/environment.ini"
    if os.path.isfile(tenant_config_file):
        click.echo(f"'{tenant_config_file}' already exists.")
    else:
        base_url = base_url.rstrip('/')
        fqdn = urlparse(base_url).netloc
        j2_env = Environment(loader = FileSystemLoader(template_dir))
        template = j2_env.get_template('environment.j2')
        environment = template.render({
                    'LIBERAFORMS_VERSION': liberaforms_version,
                    'CONTAINER_NAME': container_name,
                    'FLASK_CONFIG': "production",
                    'FLASK_DEBUG': False,
                    'LOG_LEVEL': 'INFO',
                    'FQDN': fqdn,
                    'DB_HOST': os.environ['POSTGRES_CONTAINER_NAME'],
                    'DB_NAME': constants.tenant_database_name(fqdn),
                    'DB_USER': tenant_name,
                    'DB_PASSWORD': utils.random_string(),
                    'BASE_URL': base_url,
                    'SECRET_KEY': secrets.token_hex(),
                    'CRYPTO_KEY': Fernet.generate_key().decode('ascii'),
                    'ROOT_USER_EMAIL': admin_email,
                    'SESSION_KEY_PREFIX': tenant_name,
                    'DEFAULT_LANGUAGE': os.environ['DEFAULT_TENANT_LANGUAGE'],
                    'DEFAULT_TIMEZONE': os.environ['DEFAULT_TENANT_TIMEZONE'],
                    })
        with open(tenant_config_file, 'w') as fd:
            fd.write(environment)
        click.echo(f"Please edit '{tenant_config_file}'")

def create_db(tenant_name):
    environment = constants.tenant_config(tenant_name)
    if not environment:
        click.echo(f"Cannot find tenant '{tenant_name}'.")
        return
    db_host = environment['DB_HOST']
    db_name = environment['DB_NAME']
    db_user = environment['DB_USER']
    db_password = environment['DB_PASSWORD']
    pg_cmd = f"docker exec {db_host} psql -U postgres -c".split()

    sql=f"CREATE USER {db_user} WITH PASSWORD '{db_password}';"
    cmdline = pg_cmd + [sql]
    click.echo(" ".join(cmdline))
    utils.run_subprocess(cmdline)

    sql=f"CREATE DATABASE {db_name} ENCODING 'UTF8' TEMPLATE template0;"
    utils.run_subprocess(pg_cmd + [sql])

    sql=f"GRANT ALL PRIVILEGES ON DATABASE {db_name} TO {db_user};"
    utils.run_subprocess(pg_cmd + [sql])

def upgrade_db(tenant_name):
    environment = constants.tenant_config(tenant_name)
    if not environment:
        click.echo(f"Cannot find tenant '{tenant_name}'.")
        return
    container_name = constants.tenant_container_name(tenant_name)
    flask_cmd = f"docker exec {container_name} flask db".split()

    cmdline = flask_cmd + ['upgrade']
    click.echo(" ".join(cmdline))
    utils.run_subprocess(cmdline)

def drop_db(tenant_name):
    environment = constants.tenant_config(tenant_name)
    if not environment:
        click.echo(f"Cannot find tenant '{tenant_name}'.")
        return
    db_host = environment['DB_HOST']
    db_name = environment['DB_NAME']
    db_user = environment['DB_USER']
    pg_cmd = f"docker exec {db_host} psql -U postgres -c".split()
    sql = f"DROP DATABASE {db_name};"
    utils.run_subprocess(pg_cmd + [sql])

    sql = f"DROP USER IF EXISTS {db_user}"
    utils.run_subprocess(pg_cmd + [sql])

def compose(tenant_name, compose_args):
    tenant_dir = constants.tenant_dir(tenant_name)
    compose_file = f"{tenant_dir}/docker-compose.yml"
    env_file = f"{tenant_dir}/environment.ini"
    if not os.path.isfile(env_file):
        click.echo(f"Cannot find tenant '{tenant_name}'.")
        return
    compose_cmd = [
                os.environ['DOCKER_COMPOSE_BIN'],
                "-f", compose_file,
                "--env-file", env_file
                ]
    cmdline = compose_cmd + list(compose_args)
    click.echo(" ".join(cmdline))
    environment = constants.tenant_config(tenant_name)
    version = environment['LIBERAFORMS_VERSION']
    utils.run_subprocess(cmdline, env={"LIBERAFORMS_VERSION":version})

def flask(tenant_name, flask_args):
    environment = constants.tenant_config(tenant_name)
    if not environment:
        click.echo(f"Cannot find tenant '{tenant_name}'.")
        return
    container_name = environment['CONTAINER_NAME']
    cmdline = f"docker exec {container_name} flask"
    cmdline = cmdline.split() + list(flask_args)
    click.echo(" ".join(cmdline))
    utils.run_subprocess(cmdline)
