#! /usr/bin/env python

"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os, sys
import shutil
from dotenv import load_dotenv
from jinja2 import Environment, FileSystemLoader
import click
import scripts

TENANTS_DIR = "./tenants"

load_dotenv(dotenv_path="./.env")

@click.group()
def cli():
    pass


@click.command(context_settings=dict(ignore_unknown_options=True))
@click.argument('cmd')
@click.option('--tenant', 'tenant')
@click.option('--dump-file', 'dump_file')
@click.argument('compose_args', nargs=-1, type=click.UNPROCESSED)
def postgres(cmd, compose_args, tenant=None, dump_file=None):
    if cmd == "compose":
        scripts.postgres.compose(compose_args)
    if cmd == "create-config":
        scripts.postgres.create_config()
    if cmd == "backup":
        if not (tenant):
            click.echo("--tenant is required")
            return
        scripts.postgres.backup_database(tenant)
    if cmd == "restore":
        if not (tenant and dump_file):
            click.echo("--tenant and --dump-file are required")
            return
        scripts.postgres.restore_database(tenant, dump_file)

@click.command(context_settings=dict(ignore_unknown_options=True))
@click.argument('cmd')
@click.argument('other_args', nargs=-1, type=click.UNPROCESSED)
def nginx(cmd, other_args):
    if cmd == "compose":
        scripts.nginx.compose(other_args)
    if cmd == "create-config":
        scripts.nginx.create_config()


@click.command(context_settings=dict(ignore_unknown_options=True))
@click.argument('cmd')
@click.argument('other_args', nargs=-1, type=click.UNPROCESSED)
def memcached(cmd, other_args):
    if cmd == "compose":
        scripts.memcached.compose(other_args)


@click.command(context_settings=dict(ignore_unknown_options=True))
@click.argument('tenant_name')
@click.argument('cmd')
@click.argument('other_args', nargs=-1, type=click.UNPROCESSED)
@click.option(  '--base-url', 'base_url',
                help="Fully qualified domain name.")
@click.option(  '--admin-email', 'email',
                help="The tenant's email address")
def tenant(cmd, tenant_name, email, base_url, other_args):
    if cmd == "config-create":
        scripts.tenant.create_config(tenant_name, base_url, email)
    if cmd == "compose":
        scripts.tenant.compose(tenant_name, other_args)
    if cmd == "flask":
        scripts.tenant.flask(tenant_name, other_args)
    if cmd == "database-upgrade":
        scripts.tenant.upgrade_db(tenant_name)
    if cmd == "database-create":
        scripts.tenant.create_db(tenant_name)
    if cmd == "database-drop":
        scripts.tenant.drop_db(tenant_name)


@click.command(context_settings=dict(ignore_unknown_options=True))
@click.argument('cmd')
@click.argument('compose_args', nargs=-1, type=click.UNPROCESSED)
def monitoring(cmd, compose_args):
    if cmd == "compose":
        scripts.monitoring.compose(compose_args)
    if cmd == "create-config":
        scripts.monitoring.create_config()


cli.add_command(postgres)
cli.add_command(nginx)
cli.add_command(memcached)
cli.add_command(tenant)
cli.add_command(monitoring)

if __name__ == "__main__":
    cli()
