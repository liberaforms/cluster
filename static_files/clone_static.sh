#!/bin/bash


if [ -z "${VERSION}" ] ; then
  echo "No VERSION"
  exit 1
fi

if [ -d "/opt/static/${VERSION}" ]; then
  echo "rm -fr /opt/static/${VERSION}"
  rm -fr /${VERSION}
fi
mkdir -p /opt/static/${VERSION}

if [ -d "/opt/clone_dir" ]; then
  rm -fr /opt/clone_dir
fi

git clone --depth=1 https://gitlab.com/liberaforms/liberaforms.git /opt/clone_dir

cd /opt/clone_dir
echo "Fetching v${VERSION}"
git fetch origin tag v${VERSION} --no-tags
git checkout v${VERSION} -b ${VERSION}


echo "Copying v${VERSION} static files"
cp -a /opt/clone_dir/liberaforms/static/* /opt/static/${VERSION}/
rm -fr /opt/clone_dir

echo "# ls -l /opt/static"
ls -l /opt/static
