
# Cluster monitoring

## Monitoring services

The monitoring services configuration is based on [a project by Brian Christner](https://github.com/vegasbrianc/prometheus) that is released under the terms of the MIT license. We added some changes that integrate the configuration with the `./manage.py` script and prepare the configuration we need for Liberaforms.

The monitoring consists of several containers working together:

* `node-exporter`: Provides Hardware and Operating System metrics.
* `alert-manager`: Handles the alerts sent by client applications like the `prometheus` server (not in use yet).
* `cadvisor`: Provides metrics of the resource usage of the containers on the machine.
* `prometheus`: Collects and stores the metrics obtained from the other monitoring services and tenants.
* `grafana`: Plots `prometheus`' data in dashboards.
* `victoria-metrics`: Long term LiberaForms statistics retention

### Grafana

#### Admin password

There are two ways to change `grafana`'s password:

1. Before the service runs, edit the `monitoring/grafana/config.monitoring` file and change the admin password to the desired value. **NOTE**: if the service was already created, the change will not take effect because the password will be already installed in the database.

2. Once the service is up and running, access `grafana`'s web interface and change the administration password. The default administration credentials are: `admin` / `CHANGEME`.

#### Nginx config

Edit `./monitoring/grafana/config.monitoring`

The Nginx containers will configure Grafana's VHOST and LetsEncrypt cert.

Change `grafana.my.domain` for your grafana FQDN

```
# Required by nginxproxy/acme-companion
LETSENCRYPT_HOST=grafana.my.domain

# Required by nginxproxy/docker-gen
VIRTUAL_HOST=grafana.my.domain
```

### Alertmanager

The alert manager needs to be able to send alerts when prometheus asks it to.

Copy the config.yml file

```
cp monitoring/alertmanager/config.yml.example monitoring/alertmanager/config.yml
```

Edit the `monitoring/alertmanager/config.yml` file and fix the default configuration in order to be able to receive the emails. You need to configure an SMTP service as you would in any email client.

### Create configuration

Copy the docker-compose example file and edit as needed
```
cp monitoring/docker-compose.yml.example monitoring/docker-compose.yml
```

Create the monitoring services' configuration:

```
./manage.py monitoring create-config
```

This command will populate the configuration for `prometheus` reading all the information of the tenants from the `tenants` folder and will populate `grafana`'s dashboard automatically.

**NOTE:** If new tenants are created the command must be re-run to make `prometheus` know about the existence of the new tenants.

### Start / stop monitoring

```
./manage.py monitoring compose up [-d]
./manage.py monitoring compose down
```

### Security

All monitoring services are connected internally. No ports are published. If you need to check the configuration of any of them in the web interface you must change the corresponding `docker-compose.yml` and change the port configuration.


## Victoria metrics

The victoria metrics container retains long term LiberaForms statistics

You need to install `jq`
```
apt-get install jq
```

The script will save the snapshot here (check permissions so you can write there)
```
mkdir /var/backups/victoria_db
```

Set up a cronjob to save a snapshot say, once a week
```
0 2 * * 0 /path/to/cluster/scripts/victoriadb_backup.sh
```
